
import os

#import django settings
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bootmetnt.settings")

#connect to aplication on server start
# without it scrapy items wont be able to acces models from django
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

#import models from django after connecting to app
from scrapy_djangoitem import DjangoItem
from curls.models import Words

# fields for this items are automatically created from the django model
class ArticleWords(DjangoItem):
    django_model = Words
