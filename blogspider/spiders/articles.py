import scrapy
from string import digits
import unicodedata
from blogspider.items import ArticleWords

#scrapy middleware deltafetch checks if pages was scraped before

class ArticlesSpider(scrapy.Spider):
    name = "articles"
    start_urls = {
        'https://teonite.com/blog/',
    }


    def parse(self, response):
        lLinks = response.xpath("//a[@class='read-more']/@href").extract()
        #load links to blogposts in list

        for i in range(len(lLinks)):
            #make total url
            lLinks[i] = 'https://teonite.com' + lLinks[i]

        for i in range(len(lLinks)):
            #parse every link to blog post
            url = lLinks[i]
            yield scrapy.Request(url, callback=self.ParseArticle)

        next_posts =  response.xpath("//a[@class='older-posts']/@href").extract()
        #load links to next page with urls
        if next_posts:
            # make total url, and parse links from next page
            #there are two links to next so we need to specify element or it will give error
            next_posts = 'https://teonite.com' + str(next_posts[0])
            yield scrapy.Request(url=next_posts, callback=self.parse)



    def ParseArticle(self, response):
        table = str.maketrans("!?=+().,-:/;''“”@", 17 * " ") #list of special character to remove from string
        removeDigits = str.maketrans('' ,'', digits)

        #extract data from response
        output = response.xpath(".//section[@class='post-content']//*[not(self::code)]/text()").extract()
        authornameRaw = ''.join(response.xpath("//section[@class='author']//h4/text()").extract())

        # ------ filter scrapped data ----------
        blogpost = ' '.join(output) #join to string
        blogpost= blogpost.replace('\n','').replace('"','') #remove \n and " symbols
        blogpost = blogpost.translate(table).translate(removeDigits) #remove rest symbols, “” is not ""
        blogpost = blogpost.strip() #remove multiple spaces


        authorname = str(unicodedata.normalize('NFKD', authornameRaw).replace(u'ł', 'l').replace(u'Ł', 'L').encode('ascii', 'ignore'))
        authorname = authorname[2:-1]
        #replace polish special characters
        authornameUrl = ''.join(authorname.lower()).replace(' ','')

        dataList = blogpost.split()  # split blogpost to list of words

        # ------ start saving data ---------

        articleWords = ArticleWords

        for i in range(len(dataList)):
             yield articleWords(
                 authorname=authorname,
                 authorurl=authornameUrl,
                 data=dataList[i])



